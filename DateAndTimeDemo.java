import java.time.LocalDate;
	import java.time.LocalDateTime;
	import java.time.LocalTime;
	import java.time.ZoneId;

	public class DateAnddTimeDemo {
			
		public static void main(String args[]) {
			System.out.println("Current Date & Time");
			
			LocalDate date = LocalDate.now();
			System.out.println(date);
			
			LocalTime time = LocalTime.now();
			System.out.println(time);
			
			LocalDateTime dateTime= LocalDateTime.now();
			System.out.println(dateTime);
			
			ZoneId zoneIdNY= ZoneId.of("America/New_York");
			LocalTime NYtime= LocalTime.now(zoneIdNY);
			System.out.println("New York : " + NYtime);

			ZoneId zoneIdSing= ZoneId.of("Singapore");
			LocalTime SingTime= LocalTime.now(zoneIdSing);
			System.out.println("Singapore : " + SingTime);
			
			System.out.println("Italy :" + LocalTime.now(ZoneId.of("GMT+2")));
			System.out.println("Germany :" + LocalTime.now(ZoneId.of("GMT+1")));
			System.out.println("Canada  :" +LocalTime.now(ZoneId.of("GMT-4")));
		}
	}

/*
Current Date & Time
2020-05-09
21:37:20.932
2020-05-09T21:37:20.933
New York : 12:07:20.934
Singapore : 00:07:20.984
Italy :18:07:20.984
Germany :17:07:20.984
Canada  :12:07:20.984
/
